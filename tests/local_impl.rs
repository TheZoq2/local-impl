use local_impl::local_impl;

#[local_impl]
impl<T> VecExt for Vec<T> {
    fn not_empty(&self) -> bool {
        !self.is_empty()
    }
}

#[test]
fn vec_self_reference() {
    let v1: Vec<()> = vec![];
    assert!(!v1.not_empty());
    let v2 = vec![()];
    assert!(v2.not_empty());
}

#[local_impl]
impl<T: Default> VecExt2 for Vec<T> {
    fn push_default(&mut self) {
        self.push(T::default());
    }
}

#[test]
fn vec_self_mut_reference() {
    let mut v1: Vec<()> = vec![];
    v1.push_default();
    assert_eq!(v1, vec![()]);
}

mod inner {
    pub struct S;
}

struct S;

#[local_impl]
impl SExt for S {
    fn new() -> Self {
        Self
    }
}

#[local_impl]
impl InnerSExt for inner::S {
    fn new() -> Self {
        Self
    }
}

#[test]
fn local_impl_in_same_crate() {
    let _ = S::new();
    let _ = inner::S::new();
}
